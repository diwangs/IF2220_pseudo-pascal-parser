program a;

uses
  Classes, sysutils; 

type
  parsestr = record
    str  : TStringList;
  end;
  prodvar = record
    left : array [0..1000] of string;
    right : array [0..1000,1..2] of string;
    Neff : integer;
  end;
  prodterm = record
    left : array [0..1000] of string;
    right : array [0..1000] of string;
    Neff : integer;
  end;
  cykTable = record
    Tab : array of array of TStringList;
  end;

var
  ff : TextFile;
  i, j, k : integer;
  s : parsestr;
  p1 : prodvar;
  p2 : prodterm;
  cyk : cykTable;
  z : string;

const
  sym = [';','.',':','=','(',')',',','[',']'];

procedure SplitText(aDelimiter: Char; const s: String; aList: TStringList);
begin
  aList.Delimiter := aDelimiter;
  aList.StrictDelimiter := True; // Spaces excluded from being a delimiter
  aList.DelimitedText := s;
end;

procedure addprodvar(aList: TStringList);
begin
  inc(p1.Neff);
  p1.left[p1.Neff] := aList[0];
  p1.right[p1.Neff][1] := aList[1];
  p1.right[p1.Neff][2] := aList[2];
end;

procedure addprodterm(aList: TStringList);
begin
  inc(p2.Neff);
  p2.left[p2.Neff] := aList[0];
  p2.right[p2.Neff] := aList[1];
end;

function isSymbol(x : char):boolean;
const
  sym = [';','.',':','=','(',')',','];
begin
  isSymbol := x in sym;
end;

procedure parseCNF();
var
  f : TextFile;
  ta : TStringList;
  temp : string;
begin
  assign(f, 'grammar.txt');
  reset(f);
  ta := TStringList.Create;

  while not(eof(f)) do
    begin
      readln(f, temp);
      temp  := stringreplace(temp, ' -> ', ' ',[rfReplaceAll, rfIgnoreCase]);
      SplitText(' ', temp, ta);

      if (ta.count = 3) then
        addprodvar(ta)
      else if (ta.count = 2) then
        addprodterm(ta);
    end;

  close(f);
end;

procedure parseCODE();
var
  f : TextFile;
  ta : TStringList;
  temp : string;
  c : char;
begin
  assign(f, 'code.txt');
  reset(f);
  ta := TStringList.Create;

  while not eof(f) do
	  begin
  	  readln(f, temp);

      if (ord(temp[0]) <> 0) then
        begin
          temp := stringreplace(temp, #9, ' ',[rfReplaceAll, rfIgnoreCase]);

          {i := 0;
          while (i < length(temp)) do
            begin
              if (isSymbol(temp[i])) then
                begin
                  temp := stringreplace(temp, temp[i],' '+temp[i]+' ',[rfReplaceAll, rfIgnoreCase]);
                  inc(i,2);
                end;
              inc(i);
            end;}

          for c in sym do
            begin
              temp := stringreplace(temp, c,' '+c+' ',[rfReplaceAll, rfIgnoreCase]);
            end;

          SplitText(' ',temp,ta);

          for i := 0 to ta.Count-1 do
            begin
              if (length(ta[i]) > 0) then
                s.str.append(ta[i]);
            end;
        end;
    end;

  Close(f);
end;

procedure searchProdterm(aList : TStringList; x : string);
var
  j : integer;
begin
  for j := 1 to p2.Neff do
    if (x = p2.right[j]) then
      aList.append(p2.left[j]);
  if (aList.Count = 0) then
    begin
      aList.append('NAME');
      aList.Append('EXPRESSION');
    end;
end;

procedure searchProdvar(aList : TStringList; x, y : string);
var
  j, n : integer;
  temp : string;
begin
  for j := 1 to p1.Neff do
    if (x = p1.right[j][1]) and (y = p1.right[j][2]) then
      begin
        temp := p1.left[j];
        n := aList.IndexOf(temp);
        if (n = -1)then
          aList.append(temp);
      end;
end;

procedure getCYK(var O : TStringList; i1,i2 : TStringList);
var
  X,i,j : integer;
  temp : string;
begin
  if (i1.Count <> 0) and (i2.Count <> 0) then
    begin
      for i := 0 to i1.Count-1 do
        begin
          for j := 0 to i2.Count-1 do
            begin
              searchProdvar(O, i1[i], i2[j]);
            end;
        end;
    end;
end;

// MAIN PROGRAM =================================================================
begin
s.str := TStringList.Create;
p1.Neff := 0;
p2.Neff := 0;
parseCNF();
parseCODE();

setLength(cyk.Tab,s.str.Count,s.str.Count);

for i := 0 to s.str.Count-1 do
  for j := 0 to s.str.Count-1 do
    begin
      cyk.Tab[i][j] := TStringList.Create;
      cyk.Tab[i][j].Delimiter := '|';
    end;

for i := 0 to s.str.Count-1 do
  begin
    searchProdterm(cyk.Tab[0][i], s.str[i]);
  end;

for i := 1 to s.str.Count-1 do
  begin
    for j := 0 to s.str.count-i-1 do
      begin
        for k := 0 to i-1 do
          getCYK(cyk.Tab[i][j],cyk.Tab[k][j],cyk.Tab[i-k-1][j+k+1]);
      end;
  end;

assign(ff, 'a.csv');
rewrite(ff);

for i := s.str.Count-1 downto 0 do
  begin
    z := '';
    for j := 0 to s.str.Count-i-1 do
      begin
        if (cyk.Tab[i][j].Count <> 0) then
          begin
            write(ff, cyk.Tab[i][j].DelimitedText);
            write(cyk.Tab[i][j].DelimitedText+' ');
          end
        else
          write('X ');
        write(ff, ',');
      end;
    writeln(ff,'');
    writeln();
  end;

close(ff);

end.