# IF2220_pseudo-pascal-parser
A program to parse a pseudocode that is a subset of Pascal programming language.

## Author
* Senapati Sang Diwangkara
* [M. Abdullah Munir](https://github.com/mabdullahmunir)
* [Restu Wahyu Kartiko](https://github.com/restukartiko03)